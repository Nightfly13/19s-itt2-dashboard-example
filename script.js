var baseUrl = 'http://localhost:5000/'

$(document).ready(function(){

    var ctx = document.getElementById("myChart1").getContext('2d');
    window.iot_graphs = new Chart(ctx, config1);
    var ctx = document.getElementById("myChart2").getContext('2d');
    window.iot_graphs = new Chart(ctx, config2);

});

//HTTP FUNCTIONS
async function httpGet(url){
    try {
        let response = await fetch(url);
        updateLastMessage(`${response.status} ${response.statusText}`);
        let json = await response.json();
        return json;
    } catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

async function httpPut(url, data) {
    try {
        let response = await fetch(url, {
            method: 'PUT', // 'GET', 'PUT', 'DELETE', etc.
            body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
            headers: new Headers({
                'Content-Type': 'application/json;charset=UTF-8'
            })
        });
        updateLastMessage(`${response.status} ${response.statusText}`);
        return response;
    }
    catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

//OUTPUTS
function updateElement(data, url){
    let element = document.getElementById('status-field-'+url.substr(-2)); //element status-field-A0 etc. 
    value = data.value;
    element.innerHTML = value;
    parseInt(value) ? element.style.backgroundColor = "darkolivegreen" : element.style.backgroundColor = "tomato";
}

function updateLastMessage(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text}`));
    $('#api-log').prepend(li);
}

function addData(chart, label, data) {
    //console.log(chart);
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
        if (dataset.data.length > 10) {
            removeData(chart)
        };  
    });    
    chart.update();
}

function removeData(chart) {
    chart.data.labels.shift();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.shift();
    });
    chart.update();
}

//CHART CONFIG
var config1 = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: '2.4 GHz',
            fill: false,
            backgroundColor: 'rgb(75, 192, 0)',
            borderColor: 'rgb(75, 192, 0)',
            data: [
            ]					
        }
    ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: '2.4GHz Band Signal Strength'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Timestamp'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Signal Strength'
                }
            }]
        }
    }
};
var config2 = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: '5 GHz',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
            ]					
        }
    ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: '5GHz Band Signal Strength'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'TestField1'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'TestField2'
                }
            }]
        }
    }
};

//GET TIME
function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}